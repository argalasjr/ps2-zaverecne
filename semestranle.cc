// Predmet:	Pocitacove siete 2 
// Autor:	Ondrej Rimovský 72513
//
// Zadanie 2 - Simulacia vzdialenej hliadky
// 	 Nastavte vhodnu fyzicku interpretaciu prostredia pomocou modelov definovanych v dokumentacii NS3.
// 	 Najdenie optimalneho rozlozenia AP
// 	 UAV sa pohybuje po sklade. Zvolte vhodny model pohybu/pohybov.
// 	 Vyberte a nastavte vhodny MAC protokol (WifiManager), upravte nastavenia.
// 	 Vyberte a nastavte vhodny/e smerovaci/ie protokol/y, upravte nastavenia.
// 	 Vyberte a nastavte vhodny/e transportny/e protokol/y, upravte nastavenia.
// 	 Aplikacna cast:
// 	 Robot (UAV alebo ine) sa pohybuje po sklade a vysiela udaje na server. V sklade sa nachadzajú AP s priamim pristupom na server.
// 	 priklad na udalost: Ak dron nedokaze vysielat signal vrati sa domov-vychodiskovy bod pokryty AP, a pod.
// Na vizualizaciu simulacie vyuzite program a modul NetAnim. Zadefinujte aspon 2 merane parametre QoS, 
// ktore vynesiete do grafov za pomoci modulu a programu Gnuplot. Simulaciu spustite viac krat, mente pomocou SeedManager nastavenia 
// generovania nahodnej premennej, aby ste ziskali standardnu odchylku merania (Errorbars) pre vynesene body grafu. Vhodne stanovte cas-trvanie simulacie.
// Priklad QoS (pomer priemerneho poctu poslanych uzitocnych udajov k celkovemu poctu poslanych udajov na jednu cestu/balik), 
// priklad parametra pocet vedlajsich komunikacii v sieti (udrba siete, pripojenie/odpojenie, hladanie cesty, ).
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Popis OSI/ISO modelu
// 	 Pre simulaciu som vytvoril model o rozlohe 5x5 a rovnomerne rozmiestnil 23 AP. V ramci tejto plochy sa pohybuje robot, 
// 	  ktory komunikuje s najblizsim AP. Ak nastane taka udalost pri ktorej sa robot dostane mimo dosahu AP, tak sa presunie na zaciatok. 
// 	 Pouzil som protokol CSMA a prenosovy standard 802.11g medzi AP a robotom koli lepsej ryhlosti prenosu. Pre novsi protokol 802.11n nebola podpora.
// 	 Pouzil som protokol OLSR, ktory je vhodnejsi pre bezdrotovu ad hoc siet a lepsiemumu nastavovaniu. 
//   Dalej som pouzil protokol UDP pre rychlejsi prenos.
//   Na priradenie IP adries som pouzil protokol IPv4.
//   Vysledne animacie som zobrazil pomocou programu Netanim vygenerovanim prislusnych XML suborov.
//  Pomocou casovych udalosti som nastavil zaciatok pohybu robota od 10tej sekundy. 
//  Vytvoril som callbacky na zmenu pozicie robota.
//  Pocas chodu programu som menil rychlost pohybu robota.
//
//  Grafy su kazdym spustenim programu odlisne koli nastaveniu SeedManagera, ktory ovplyvnuje nahodne priradenie velkosti kroku.
//  1. graf popisuje zavislost priepustnosti(Troughput) od vzdialenosti rozlozenia AP(Grid node distance)
//     - zistil som ze optimalne rozlozenie AP je ked mam najmesiu odchylku s najvyssou priepustnostou. Pre moj graf to bola hodnota: 60 
//  2. graf popisuje zavislost priepustnosti(Troughput) od hranice fragmentacie(Fragmentation threshold)
//     - graf popisuje vplyv fragmentacie velkosti dat, ktora sa moze prejavit zvysenim priepustnosti(Troughput).

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/gnuplot.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/csma-helper.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("WifiSimpleAdhocGrid");

double freq = 0.7;

static void runSimulation(double simulTime){
    Simulator::Stop(Seconds(simulTime));
    Simulator::Run();
    Simulator::Destroy();
}

static void makeAnimations(bool anim, std::string name, NodeContainer staticAP, NodeContainer drone, double simulTime){
    if (anim)
    {
        AnimationInterface anim1(name);
        //povolenie metadat
        //anim1.EnablePacketMetadata();

        //pomenovanie grafu
        for(int ii = 0; ii < 24 ;ii++)
        {
            anim1.UpdateNodeColor(staticAP.Get(ii),0,0,0);
            anim1.UpdateNodeDescription(staticAP.Get(ii),"w");                
        }                
        anim1.UpdateNodeColor(drone.Get(0),0,255,0);
        anim1.UpdateNodeDescription(drone.Get(0),"Drone");            
        anim1.UpdateNodeColor(staticAP.Get(0),0,255,0);
        anim1.UpdateNodeDescription(staticAP.Get(0),"Server");

        runSimulation(simulTime);               

    }
    else
    {
        runSimulation(simulTime);
    }
}

static void vypis(uint32_t TxPackets, uint32_t RxPackets, double Throughput){
    std::cout << TxPackets << "\n";
    std::cout << RxPackets << "\n";
    std::cout << Throughput << "\n";
}

void ReceivePacket(Ptr<Socket> socket) {
    while (socket->Recv()) {
        //NS_LOG_UNCOND ("Received one packet!");
    }
}

static void changeL1(double f){
    Config::Set("NodeList/24/$ns3::MobilityModel/$ns3::RandomWaypointMobilityModel/Speed", StringValue("ns3::ConstantRandomVariable[Constant=40]"));
    freq = f;
}

static void CourseChange(double distance, Ptr<Node> node0, std::string context,
		Ptr<const MobilityModel> mobility) {
	Vector pos = mobility->GetPosition(); 
	
	Ptr <ConstantPositionMobilityModel> mobility2 = node0 -> GetObject<ConstantPositionMobilityModel>();
	
	if(pos.x > 5*distance || pos.y > 5*distance){
            pos = Vector (0.0,0.0,0.0);
            mobility2->SetPosition (pos);	
	}
	std::cout << " -x " << pos.x << " -y " << pos.y << std::endl;
}


static void GenerateTraffic(Ptr<Socket> socket, uint32_t pktSize,
        uint32_t pktCount, Time pktInterval) {
    if (pktCount > 0) {
        socket->Send(Create<Packet> (pktSize));
        Simulator::Schedule(pktInterval, &GenerateTraffic,
                socket, pktSize, pktCount - 1, pktInterval);
    } else {
        socket->Close();
    }
}

static void Move(Ptr<Node> node0) {
    Ptr <ConstantPositionMobilityModel> mobility = node0 -> GetObject<ConstantPositionMobilityModel>();
		Vector position = mobility -> GetPosition() ;

                time_t currentTime;
                time (&currentTime);
                RngSeedManager::SetSeed (currentTime);
                
		Ptr<UniformRandomVariable> rand2 = CreateObject<UniformRandomVariable>();
		rand2->SetAttribute( "Min", DoubleValue( 100) );
		rand2->SetAttribute( "Max", DoubleValue( 400) );

		position = position + Vector (rand2->GetInteger(),rand2->GetInteger(),0.0);
	
    mobility->SetPosition (position);

    Simulator::Schedule (Seconds (freq), Move, node0);
}



int main(int argc, char *argv[]) {
    std::string phyMode("DsssRate1Mbps");
    double distance = 500; // m
    uint32_t packetSize = 1000; // bytes
    uint32_t numPackets = 1000;
    double interval = 0.010; // seconds
    bool verbose = false;
    double Throughput = 0.0;
    bool anim = false;
    double simulTime = 33.0;
    CommandLine cmd;
    
    cmd.AddValue ("anim", "zapisovanie animacie [true/false], default =", anim);
    cmd.AddValue ("simulTime", "nastavenie casu simulacie, default =", simulTime);
    cmd.Parse(argc, argv);
    
    // Convert to time object
    Time interPacketInterval = Seconds(interval);

    // disable fragmentation for frames below 2200 bytes
    Config::SetDefault("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue("2200"));
    // turn off RTS/CTS for frames below 2200 bytes
    Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("2200"));
    // Fix non-unicast data rate to be the same as that of unicast
    Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode",
            StringValue(phyMode));

    Gnuplot graf("graf.svg");
    graf.SetTerminal("svg");
    graf.SetLegend("Grid nodes distance", "Throughput [Mbit/s]");

    Gnuplot2dDataset dataset1;
    dataset1.SetTitle("Throughput");
    dataset1.SetStyle(Gnuplot2dDataset::LINES_POINTS);
    dataset1.SetErrorBars(Gnuplot2dDataset::Y);
    
    int numPoints = 10;
    double dataAll[numPoints][numPoints];
    double distances[numPoints];
    int index = 0;
    //    double dataAll2[10][5];

    for (int run = 0; run < numPoints; run++) {
        for (distance = 65; distance <= 650; distance += 65) {
            uint32_t TxPackets = 0;
            uint32_t RxPackets = 0;
            std::cout << "***distance: " << distance << " ***" << "\n";
			
            NodeContainer c;
            NodeContainer staticAP;
            NodeContainer drone;
            staticAP.Create(24);
            drone.Create(1);
            NodeContainer server;

            c.Add(staticAP);
            c.Add(drone);
            
            NodeContainer ethernet;
            ethernet.Add(server);
            ethernet.Add(staticAP);

            // The below set of helpers will help us to put together the wifi NICs we want
            WifiHelper wifi;
            if (verbose) {
                wifi.EnableLogComponents(); // Turn on all Wifi logging
            }

            YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
            // set it to zero; otherwise, gain will be added
            wifiPhy.Set("RxGain", DoubleValue(-10));
            // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
            wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

            YansWifiChannelHelper wifiChannel;
            wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel");
            wifiPhy.SetChannel(wifiChannel.Create());

            // Add an upper mac and disable rate control
            WifiMacHelper wifiMac;
            wifi.SetStandard(WIFI_PHY_STANDARD_80211g);
            wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                    "DataMode", StringValue(phyMode),
                    "ControlMode", StringValue(phyMode));
            // Set it to adhoc mode
            wifiMac.SetType("ns3::AdhocWifiMac");
            NetDeviceContainer devices = wifi.Install(wifiPhy, wifiMac, c);

            MobilityHelper mobility;
            mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                    "MinX", DoubleValue(0.0),
                    "MinY", DoubleValue(0.0),
                    "DeltaX", DoubleValue(distance),
                    "DeltaY", DoubleValue(distance),
                    "GridWidth", UintegerValue(5),
                    "LayoutType", StringValue("RowFirst"));
            mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
            mobility.Install(staticAP);
            MobilityHelper mobility2;
            Ptr<ListPositionAllocator> positionAlloc = CreateObject <ListPositionAllocator>();
            positionAlloc ->Add(Vector(250, 250, 0));

            mobility2.SetPositionAllocator(positionAlloc);
            //mobility2.SetMobilityModel("ns3::RandomDirection2dMobilityModel",
            //        "Speed", StringValue("ns3::ConstantRandomVariable[Constant=200.0]"),
            //        "Bounds", StringValue("0|5000|0|5000"));
						mobility2.SetMobilityModel("ns3::ConstantPositionMobilityModel");
            mobility2.Install(drone);



            // Enable OLSR
            OlsrHelper olsr;
            Ipv4StaticRoutingHelper staticRouting;

            Ipv4ListRoutingHelper list;
            list.Add(staticRouting, 0);
            list.Add(olsr, 10);

            InternetStackHelper internet;
            internet.SetRoutingHelper(list); // has effect on the next Install ()
            internet.Install(c);

            Ipv4AddressHelper ipv4;
            NS_LOG_INFO("Assign IP Addresses.");
            ipv4.SetBase("10.1.1.0", "255.255.255.0");
            Ipv4InterfaceContainer in = ipv4.Assign(devices);

            TypeId tid = TypeId::LookupByName("ns3::UdpSocketFactory");
            for (int i = 0; i < 24; i++) {
                Ptr<Socket> recvSink = Socket::CreateSocket(staticAP.Get(i), tid);
                InetSocketAddress local = InetSocketAddress(in.GetAddress(24, 0), 80);
                recvSink->Bind(local);
                recvSink->SetRecvCallback(MakeCallback(&ReceivePacket));
            }

            for (int i = 0; i < 24; i++) {
                Ptr<Socket> source3 = Socket::CreateSocket(drone.Get(0), tid);
                InetSocketAddress remote = InetSocketAddress(in.GetAddress(i, 0), 80);
                source3->SetAllowBroadcast(false);
                source3->Connect(remote);
                // Give OLSR time to converge-- 30 seconds perhaps
                Simulator::Schedule(Seconds(10.0), &GenerateTraffic, source3, packetSize, numPackets, interPacketInterval);
            }
            
            ///CALLBACKs & SCHEDULER
            Simulator::Schedule (Seconds (10), Move, drone.Get (0)); 
            Simulator::Schedule(Seconds(25), changeL1,0.7);
            Simulator::Schedule(Seconds(15), changeL1,0.2);
            Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",  MakeBoundCallback(&CourseChange , distance, drone.Get (0)));
  
            FlowMonitorHelper flowmon;
            Ptr<FlowMonitor> monitor = flowmon.InstallAll();

            makeAnimations(anim, "simul1.xml", staticAP, drone,simulTime);
           
            
            monitor->CheckForLostPackets();
            Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier());
            FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats();
            for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i) {
                TxPackets = i->second.txPackets;
                RxPackets += i->second.rxPackets;
            }

//            Throughput = RxPackets * 8.0 / 33;
            Throughput = RxPackets/simulTime-10 /1000/1000;

            dataAll[run][index] =  Throughput;
            distances[index] = distance;
            index++;

            vypis(TxPackets, RxPackets, Throughput);
        }
        index = 0;
    }
    
    std::cout << "pocitame" << "\n";
     
    double total, mean, std;
    for (int distanceIndex = 0; distanceIndex < numPoints; distanceIndex++) {
        total = 0;
        std = 0;
        for (int run = 0; run < numPoints; run++) {
            total += dataAll[run][distanceIndex];
        }
        mean = total / numPoints;
        for (int run = 0; run < numPoints; run++) {
            std += pow(dataAll[run][distanceIndex] - mean, 2);
        }
        std = sqrt(std / numPoints);
        dataset1.Add(distances[distanceIndex], mean, std);
    }
    graf.AddDataset(dataset1);
    std::ofstream plotFile("graf.plt");
    graf.GenerateOutput(plotFile);
    plotFile.close();

    if (system("gnuplot graf.plt"));
    
    //graf  2
    
    Gnuplot graf2("graf2.svg");
    graf2.SetTerminal("svg");
    graf2.SetLegend("FragmentationThreshold", "Throughput [Mbit/s]");

    Gnuplot2dDataset dataset2;
    dataset2.SetTitle("Throughput");
    dataset2.SetStyle(Gnuplot2dDataset::LINES_POINTS);
    dataset2.SetErrorBars(Gnuplot2dDataset::Y);

    double dataAll2[10][10];
    double fragmentationThresholdsDouble[10] = {2000.0, 2250.0, 2500.0, 2750.0, 3000.0, 3250.0, 3500.0, 3750.0, 4000.0, 4250.0};
    std::string fragmentationThresholds[10] = {"2000", "2250", "2500", "2750", "3000", "3250", "3500", "3750", "4000", "4250"};
    
    for (int run2 = 0; run2 < 10; run2++) {
        for (int idx2 = 0; idx2 < 10; idx2++) {
            Config::SetDefault("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue(fragmentationThresholds[idx2]));
    
            uint32_t TxPackets = 0;
            uint32_t RxPackets = 0;
            std::cout << "***Fragmentation Threshold: " << fragmentationThresholds[idx2] << " ***" << "\n";
			
            NodeContainer c;
            NodeContainer staticAP;
            NodeContainer drone;
            staticAP.Create(24);
            drone.Create(1);
            NodeContainer server;

            c.Add(staticAP);
            c.Add(drone);
            
            NodeContainer ethernet;
            ethernet.Add(staticAP);
            ethernet.Add(server);

            // The below set of helpers will help us to put together the wifi NICs we want
            WifiHelper wifi;
            if (verbose) {
                wifi.EnableLogComponents(); // Turn on all Wifi logging
            }

            YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
            // set it to zero; otherwise, gain will be added
            wifiPhy.Set("RxGain", DoubleValue(-10));
            // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
            wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

            YansWifiChannelHelper wifiChannel;
            wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel");
            wifiPhy.SetChannel(wifiChannel.Create());

            // Add an upper mac and disable rate control
            WifiMacHelper wifiMac;
            wifi.SetStandard(WIFI_PHY_STANDARD_80211b);
            wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                    "DataMode", StringValue(phyMode),
                    "ControlMode", StringValue(phyMode));
            // Set it to adhoc mode
            wifiMac.SetType("ns3::AdhocWifiMac");
            NetDeviceContainer devices = wifi.Install(wifiPhy, wifiMac, c);

            MobilityHelper mobility;
            mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                    "MinX", DoubleValue(0.0),
                    "MinY", DoubleValue(0.0),
                    "DeltaX", DoubleValue(1100.0),
                    "DeltaY", DoubleValue(1100.0),
                    "GridWidth", UintegerValue(5),
                    "LayoutType", StringValue("RowFirst"));
            mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
            mobility.Install(staticAP);
            MobilityHelper mobility2;
            Ptr<ListPositionAllocator> positionAlloc = CreateObject <ListPositionAllocator>();
            positionAlloc ->Add(Vector(250, 250, 0));

            mobility2.SetPositionAllocator(positionAlloc);
            mobility2.SetMobilityModel("ns3::ConstantPositionMobilityModel");
            mobility2.Install(drone);

            // Enable OLSR
            OlsrHelper olsr;
            Ipv4StaticRoutingHelper staticRouting;

            Ipv4ListRoutingHelper list;
            list.Add(staticRouting, 0);
            list.Add(olsr, 10);

            InternetStackHelper internet;
            internet.SetRoutingHelper(list); // has effect on the next Install ()
            internet.Install(c);
            
            Ipv4AddressHelper ipv4;
            NS_LOG_INFO("Assign IP Addresses.");
            ipv4.SetBase("10.10.0.0", "255.255.255.0");
            Ipv4InterfaceContainer in = ipv4.Assign(devices);
            
            NS_LOG_INFO("Assign IP Addresses.");
            ipv4.SetBase("192.168.0.0", "255.255.255.0");
            CsmaHelper csma;
            csma.SetChannelAttribute("DataRate",
                    DataRateValue(DataRate(50000 * 1000)));
            csma.SetChannelAttribute("Delay", TimeValue(MilliSeconds(50)));
            NetDeviceContainer Devices = csma.Install(ethernet);

            internet.Install(server);

            TypeId tid = TypeId::LookupByName("ns3::UdpSocketFactory");
            for (int i = 0; i < 24; i++) {
                Ptr<Socket> recvSink = Socket::CreateSocket(staticAP.Get(i), tid);
                InetSocketAddress local = InetSocketAddress(in.GetAddress(24, 0), 80);
                recvSink->Bind(local);
                recvSink->SetRecvCallback(MakeCallback(&ReceivePacket));
            }

            for (int i = 0; i < 24; i++) {
                Ptr<Socket> source3 = Socket::CreateSocket(drone.Get(0), tid);
                InetSocketAddress remote = InetSocketAddress(in.GetAddress(i, 0), 80);
                source3->SetAllowBroadcast(false);
                source3->Connect(remote);
                Simulator::Schedule(Seconds(10.0), &GenerateTraffic, source3, packetSize, numPackets, interPacketInterval);
            }
            Simulator::Schedule (Seconds (10), Move, drone.Get (0)); 
            Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",  MakeBoundCallback(&CourseChange , 1100.0, drone.Get (0)));
  
            FlowMonitorHelper flowmon;
            Ptr<FlowMonitor> monitor = flowmon.InstallAll();

            
            makeAnimations(anim, "simul2.xml", staticAP, drone,simulTime);
            
            monitor->CheckForLostPackets();
            Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier());
            FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats();
            for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i) {
                TxPackets = i->second.txPackets;
                RxPackets += i->second.rxPackets;
            }

            Throughput = RxPackets/simulTime-10 /1000/1000;
            
            dataAll2[run2][idx2] = Throughput;

            
            vypis(TxPackets, RxPackets, Throughput);
        }
    }
    
    std::cout << "Nasleduje pocitanie odchylky 2" << "\n";
     
    double total2, mean2, std2;
    for (int idx2 = 0; idx2 < 10; idx2++) {
        total2 = 0;
        std2 = 0;
        for (int run2 = 0; run2 < 10; run2++) {
            total2 += dataAll2[run2][idx2];
        }
        mean2 = total2 / 10;
        for (int run2 = 0; run2 < 10; run2++) {
            std2 += pow(dataAll2[run2][idx2] - mean2, 2);
        }
        std2 = sqrt(std2 / 10);
        dataset2.Add(fragmentationThresholdsDouble[idx2], mean2, std2);
    }
    graf2.AddDataset(dataset2);
    std::ofstream plotFile2("graf2.plt");
    graf2.GenerateOutput(plotFile2);
    plotFile2.close();

    if (system("gnuplot graf2.plt"));
}