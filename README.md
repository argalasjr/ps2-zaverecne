– Nastavte vhodnú fyzickú interpretáciu prostredia pomocou modelov definovaných v dokumentácii NS3.
— Nájdenie optimálneho rozloženia AP
— UAV sa pohybuje po sklade. Zvoľte vhodný model pohybu/pohybov.
– Vyberte a nastavte vhodný MAC protokol (WifiManager), upravte nastavenia.
– Vyberte a nastavte vhodný/é smerovací/ie protokol/y, upravte nastavenia.
– Vyberte a nastavte vhodný/é transportný/é protokol/y, upravte nastavenia.
– Aplikačná časť:
— Robot (UAV alebo iné) sa pohybuje po sklade a vysiela údaje na server. V sklade sa nachadzajú AP s priamim prístupom na server.
– príklad na udalosť: Ak dron nedokáže vysielať signál vráti sa „domov“-východiskový bod pokrytý AP, a pod.

Na vizualizáciu simulácie využite program a modul NetAnim. Zadefinujte aspoň 2 merané parametre QoS, 
ktoré vynesiete do grafov za pomoci modulu a programu Gnuplot. 
Simuláciu spustite viac krát, meňte pomocou SeedManager nastavenia generovania náhodnej premennej, 
aby ste získali štandardnú odchýlku merania (Errorbars) pre vynesené body grafu. 
Vhodne stanovte čas-trvanie simulácie.

Príklad QoS (pomer priemerného počtu poslaných užitočných údajov k celkovému počtu poslaných údajov na jednu cestu/balík), 
príklad parametra počet vedľajších komunikácii v sieti (údržba siete, pripojenie/odpojenie, hľadanie cesty, …).